<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    
    protected $table = 'categories';

     protected $fillable = [

        'name','tree','isEnable','createdBy','updatedBy'
    ];

    
    public function subCategory() {

    	return $this->hasMany('App\Categories','tree');
    }

    public function getchildecategory($id)
    {
    	$category = $this->where('tree',$id)->get();
    	return $category;

    }

    public function featuredImage($id)
    {
    	$featureimage = Wallpaper::where('categoriesId',$id)->where('categoriesImage','category')->value('image');
    	return $featureimage;
    }

    public function getwallpapers($id)
    {
        $featureimage = Wallpaper::where('categoriesId',$id)->Select('id','image')->where('categoriesImage','=',NULL)->get();
        foreach ($featureimage as $row) {
            $row->image = config('app.url').'/'.$row->image;
        }
        return $featureimage;
    }


}
