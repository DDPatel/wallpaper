<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Wallpaper;

class MainController extends Controller
{
    public function categories(Request $request){
		
		$categories = Categories::whereTree('main')->get();
		$maindata = array();
		foreach ($categories as $row) {
			$subdata = array();
			foreach ($row->subCategory as $value) {
				$subdata[] = array('id' => $value->id, 'name' =>$value->name, 'image'=>config('app.url').'/'.$value->featuredImage($value->id));
			}

			$data = array('id'=>$row->id,'name'=>$row->name,'image'=>config('app.url').'/'.$row->featuredImage($row->id),'subcategory' =>$subdata);
			array_push($maindata,$data);
		}
		
        return [
        	'data' => $maindata
        ];

    }


    public function wallpapers(){

    	$categories = Categories::get();
    	$maindata = array();
    	foreach ($categories as $row) {
    		$data = array('id'=>$row->id,'name'=>$row->name,'image'=>config('app.url').'/'.$row->featuredImage($row->id),'wallpapers'=>$row->getwallpapers($row->id));
    		array_push($maindata,$data);
    	}
    	return [
        	'data' => $maindata
        ];
    }
}
