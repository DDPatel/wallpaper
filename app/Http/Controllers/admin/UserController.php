<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use Hash;
use App\Role;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = User::get();

        return view('admin.users.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getRole = Role::orderBy('name','asc')->pluck('name','id')->all();
        return view('admin.users.create',compact('getRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'password' => 'required|min:6|max:10',
        'phone_number' => 'required|min:10|max:10',
      ];

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){

        return redirect()
             ->back()
             ->withErrors($validator)
             ->withInput();

      }
  // check email already exist START
      $checkMail = User::where('email',$request->email)->first();

      if(!is_null($checkMail)){
        Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Email Already Exist.!! </div>');
        return redirect('admin/users/create')->withInput();
      }
  // check email already exist END

  // check phone already exist START
      $checkPhone = User::where('phone_number',$request->phone_number)->first();

      if(!is_null($checkPhone)){
        Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Phone Number Already Exist.!! </div>');
        return redirect('admin/users/create')->withInput();
      }
  // check phone already exist END

      $user = new User();
      $data['first_name'] = $request->first_name;
      $data['last_name'] = $request->last_name;
      $data['email'] = $request->email;
      $data['password'] = Hash::make($request->password);
      $data['phone_number'] = $request->phone_number;
      $user->fill($data);

      if($user->save()){
        $clientrole=Role::where('id',$request->userRole)->first();
			  $user->attachRole($clientrole);
        Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> User Added Successfully.!! </div>');
        return redirect('admin/users');
      }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $userId = $request->id;
        $getUserDetail = User::where('id',$userId)->first();
        return view('admin.users.edit',compact('getUserDetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = $request->id;

        $rules = [
          'first_name' => 'required',
          'last_name' => 'required',
          'phone_number' => 'required|min:10|max:10',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){

          return redirect()
               ->back()
               ->withErrors($validator)
               ->withInput();

        }

        // check phone already exist START
            $checkPhone = User::where('phone_number',$request->phone_number)->first();

            if(!is_null($checkPhone)){
              Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Phone Number Already Exist.!! </div>');
              return redirect('admin/users');
            }
        // check phone already exist END
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['phone_number'] = $request->phone_number;

        $updateUser = User::where('id',$userId)->update($data);

        Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> User Updated Successfully.!! </div>');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      $userId = $request->id;

      $deleteUser = User::where('id',$userId)->delete();
      Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> User Deleted Successfully.!! </div>');
      return redirect('admin/users');
    }

    public function checkpassword(Request $request,$id){
      $userId = $request->id;
      return view('admin.users.changepassword',compact('userId'));
    }

    public function changepassword(Request $request,$id){
      $userId = $request->id;

      $getUserPassword = User::where('id',$userId)->select('password')->first();

      $rules =[
        'new_password' => 'required',
        'confirm_password' => 'required|same:new_password',
      ];

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){

        return redirect()
             ->back()
             ->withErrors($validator)
             ->withInput();

      }

      $data['password'] = Hash::make( $request->new_password);
      $updatePassword = User::where('id',$userId)->update($data);

      Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Password Updated Successfully.!! </div>');
      return redirect('admin/users');
        }
}
