<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Wallpaper;

class CategoriesController extends Controller
{
        //load manage category view
    public function index() {

    	$categories = Categories::whereTree('main')->get();
        

    	return view('admin.categories.index',compact('categories'));
    }
    //new catagory controle
    public function newCategory(Request $req)
    {
    	$data = $req->all();

        if($data['name'] == null && $data['image'] == null ) {

            // session()->flash('result',['Can\'t create category with empty file or value.','sucess']);
            session()->flash('result',['not possible to create blank category','Opps']);

        }
        else {
             
            session()->flash('result',['Catagory Successfully Created','success']);
            $newCategory = new Categories();
            $newCategory->name = $data['name'];
            $newCategory->tree = $data['id'];
            $newCategory->save();

            $images = $req->file('image');
            
            $filename = $newCategory->id .uniqid().".".$images->getClientOriginalExtension() ;
            $images->move('public/upload/wallpaper',$filename);

            $pic = new Wallpaper();
            $pic->categoriesId = $newCategory->id;
            $pic->image = 'public/upload/wallpaper/'.$filename;
            $pic->categoriesImage = 'category';
            $pic->save();
        } 
           
        return back();
    }
    //remove category module
    public function removeCategory(Request $req) { 

            $data = $req->all();
            $deleteCategory = Categories::find($data['id']);

            
            if(!$deleteCategory->wallpaper->count()) {
                Categories::whereTree($data['id'])->update(['tree'=>$deleteCategory->tree]);
                $deleteCategory->delete();
                session()->flash('result',['Catagory Successfully Removed','success']);
            }
            else {

                session()->flash('result',['Many Ad of this type, plz remove ad first','Opps']);
            }
            return back();
    }
    //add parent category module
    public function parentCategory(Request $req) {

        $data = $req->all();

        if($data['name'] == null && $data['image'] == null) {

            // session()->flash('result',['Can\'t create category with empty value.','sucess']);
            session()->flash('result',['not possible to create blank category','Opps']);

        }
        else {
                $category = Categories::find($data['id']);
                $newCategory = new Categories();
                $newCategory->name = $data['name'];
                $newCategory->tree = $category->tree;
                $newCategory->save();
                
                $images = $req->file('image');
            
                $filename = $newCategory->id .uniqid().".".$images->getClientOriginalExtension() ;
                $images->move('public/upload/wallpaper',$filename);

                $pic = new Wallpaper();
                $pic->categoriesId = $newCategory->id;
                $pic->image = 'public/upload/wallpaper/'.$filename;
                $pic->categoriesImage = 'category';
                $pic->save();

                $category->update(['tree'=>$newCategory->id]);
                session()->flash('result',['Parent category successfully added.','success']);

        }
       
    return back();
    }

    public function wallpapers(Request $req) {
        
        $data = $req->all();

        $id = $data['id'];

	            $images = $req->file('file');
	             
		            $filename = $id .uniqid().".".$images->getClientOriginalExtension() ;
		            $images->move('public/upload/wallpaper',$filename);

		            $pic = new Wallpaper();
		            $pic->categoriesId = $id;
		            $pic->image = 'public/upload/wallpaper/'.$filename;
		            $pic->save();
  
        return back()->with('success','successfully Upload Your Products Pic..');
    }

    public function wallView($id, Categories $category) {

       $wallpapers = Wallpaper::where('categoriesId',$id)->where('categoriesImage',null)->paginate(15);

       $catwall = Wallpaper::where('categoriesId',$id)->where('categoriesImage','category')->first();

       $category = $category->where('id',$id)->first();

       return view('admin.categories.wallview',compact('wallpapers','catwall','category'));
    }
}
