<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class Role extends EntrustRole
{
  use Notifiable;
   use EntrustUserTrait;


   protected $fillable = [
       'name', 'display_name', 'description'
   ];
}
