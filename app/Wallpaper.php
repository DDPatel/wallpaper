<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallpaper extends Model
{
        
    protected $table = 'wallpapers';

     protected $fillable = [

        'categoriesId','image','download','share'
    ];
}
