<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tree');
            $table->enum('isEnable',['true','false'])->default('true');
            $table->integer('createdBy')->unsigned()->nullable();
            $table->foreign('createdBy')->references('id')->on('users');
            $table->integer('updatedBy')->unsigned()->nullable();
            $table->foreign('updatedBy')->references('id')->on('users'); 
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
