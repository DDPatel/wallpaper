<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWallpaperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallpapers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoriesId')->unsigned()->nullable();
            $table->foreign('categoriesId')->references('id')->on('categories')->onDelete('cascade');
            $table->string('image')->nullable(); 
            $table->string('categoriesImage')->nullable(); 
            $table->string('download')->nullable();
            $table->string('share')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallpapers');
    }
}
