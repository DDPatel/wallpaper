<h3> MICRA Solution Admin Theme Setup </h3>

1.Clone OR Add Project (admin_setup).
2. change name of (env.example) to (.env).
3. write command for generate Key:  php artisan key:generate.
4.update Composer Using Command:  composer update.
5.Run migration using command:  php artisan migrate.
6.run seeder using command:  php artisan db:seed
                                         OR
                           php artisan db:seed --class=(seeder class Name)
