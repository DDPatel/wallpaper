@extends('layouts.admin') 

@section('title') Micra - Manage Wallpaper @endsection 

@section('pagelevel_css')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{URL::to('admin-asset/global/plugins/cubeportfolio/css/cubeportfolio.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{URL::to('admin-asset/pages/css/portfolio.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
@endsection 

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
                            <!-- BEGIN PAGE TITLE-->
                            <h1 class="page-title"><strong>{{$category->name}}</strong></h1>
                            <!-- END PAGE TITLE-->
                            <!-- END PAGE HEADER-->
                            <div class="portfolio-content portfolio-3">
    
                                <div id="js-grid-lightbox-gallery" class="cbp">
                                @foreach($wallpapers as $raw)
                                    <div class="cbp-item web-design graphic print motion">
                                        <div class="cbp-caption " >
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="{{URL::to($raw->image)}}" alt="" style="width: 300px; height: 200px;"> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <!-- <div class="cbp-l-caption-title">World Clock Widget</div>
                                                        <div class="cbp-l-caption-desc">by Paul Flavius Nechita</div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                
                                </div>
                                @unless(count($wallpapers))
                                    <center><b>No wallpapers available here</b></center>
                                @endunless
                            </div>
                             {{ $wallpapers->links() }}
  </div>
</div>
@endsection

@section('pagelevel_plugins') 

@endsection 

@section('pagelevel_script')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{URL::to('admin-asset/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{URL::to('admin-asset/pages/scripts/portfolio-3.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
@endsection