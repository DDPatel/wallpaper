<ul class="dl-submenu">
@foreach($childs as $child)
	
	@if(count($child->subCategory))
	        <li><i class="fa fa-fighter-jet"></i>{{$child->name}}
	         <a  data-toggle="modal" href="#parentup" onClick="parent({{$child->id}})"> <i class="fa fa-reply-all"></i></a> 
	        <a data-toggle="modal" href="#small"  onClick="conformation({{$child->id}})"><i class="fa fa-plus"></i></a>
	         <a ><i data-toggle="modal" href="#close" onClick="closeup({{$child->id}})" class="fa fa-close"></i></a>
			<a><i data-toggle="modal" href="#up"  onClick="upload({{$child->id}})" class="fa fa-upload"></i></a>
			<a href="{{'wallpaper-view/'.$child->id}}"><i class="fa fa-picture-o"></i></a>
			@include('admin.categories.childview',['childs' => $child->subCategory])

            </li>
   
    @else
    	 <li>{{$child->name}}
    	  <a  data-toggle="modal" href="#parentup" onClick="parent({{$child->id}})"> <i class="fa fa-reply-all"></i></a> 
	        <a data-toggle="modal" href="#small" onClick="conformation({{$child->id}})"><i class="fa fa-plus"></i></a>
          <a ><i data-toggle="modal" href="#close" onClick="closeup({{$child->id}})" class="fa fa-close"></i></a>   
			<a><i data-toggle="modal" href="#up"  onClick="upload({{$child->id}})" class="fa fa-upload"></i></a>
			<a href="{{'wallpaper-view/'.$child->id}}"><i class="fa fa-picture-o"></i></a>
			@endif
			
@endforeach
</ul>