@extends('layouts.admin') 

@section('title') Micra - Manage Categories @endsection 

@section('pagelevel_css')
<link href="{{URL::to('admin-asset/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::to('admin-asset/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"
/>
<link href="{{URL::to('admin-asset/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet"
  type="text/css" />
<link href="{{URL::to('admin-asset/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}" rel="stylesheet" type="text/css"
/>
<link href="{{URL::to('admin-asset/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet"
  type="text/css" />
<link href="{{URL::to('admin-asset/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css"
/>
<link type="text/css" rel="stylesheet" href="{{ URL::asset('public/css/treeview.css')}}">
<link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet" type="text/css" />
<link href="{{URL::to('public/css/dropzone.css')}}" rel="stylesheet"> 

@endsection 

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p class="well" style="padding-right: 20px">
            <strong>Manage Categories</strong>
          </p>
          <a data-toggle="modal" href="#small" onClick="conformation('main')"><i class="fa fa-plus"></i></a>
          <ul class="tree">
            @foreach($categories as $category)
            <li><i class="fa fa-sitemap"></i>{{$category->name}}
              <a data-toggle="modal" href="#parentup" onClick="parent({{$category->id}})"> <i class="fa fa-reply-all"></i></a>
              <a data-toggle="modal" href="#small" onClick="conformation({{$category->id}})"><i class="fa fa-plus"></i></a>
              <a><i data-toggle="modal" href="#close" onClick="closeup({{$category->id}})" class="fa fa-close"></i></a>
              <a><i data-toggle="modal" href="#up"  onClick="upload({{$category->id}})" class="fa fa-upload"></i></a> 
              <a href="{{'wallpaper-view/'.$category->id}}"><i class="fa fa-picture-o"></i></a>
              @if(count($category->subCategory))
              @include('admin.categories.childview',['childs' => $category->subCategory]) @endif
            </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>

    <div class="modal fade bs-modal-md" id="small" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; width:100%;">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Enetr child-category</h4>
          </div>
          <div class="modal-body">
            <form action="{{ URL::to('admin/new-category')}}" method="post" enctype="multipart/form-data">
              <center>
                {{csrf_field() }} {!! Form::text('name','', ['class'=>'form-control','placeholder' => 'Add Name','style' => ' width: 53%;'])
                !!}
                <br>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="input-group input-small">
                    <div class="form-control uneditable-input input-fixed input-small" data-trigger="fileinput">
                      <i class="fa fa-file fileinput-exists pull-left"></i>&nbsp;
                      <span class="fileinput-filename"> </span>
                    </div>
                    <span class="input-group-addon btn default btn-file ">
                       <span class="fileinput-new"> Select file </span>
                    <span class="fileinput-exists"> Change </span>
                    <input type="file" name="image"> </span>
                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
                </div>
                {!! Form::hidden('id','', ['id'=>'id']) !!}
              </center>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="submit" class="btn green">Confirm</button>
            </form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-sm" id="close" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Are you sure for delete ?</h4>
          </div>
          <div class="modal-body">
            <form action="{{ URL::to('admin/remove-category')}}" method="post">
              {{csrf_field() }} {!! Form::hidden('id','', ['id'=>'closeid']) !!}
              <center>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                <button type="submit" class="btn green btn-outline">Ok</button> </center>
            </form>

          </div>

        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <div class="modal fade bs-modal-md" id="parentup" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Enter parent-category</h4>
          </div>
          <div class="modal-body">
            <form action="{{ URL::to('admin/parent-category')}}" method="post" enctype="multipart/form-data">
              <center>
                {{csrf_field() }} {!! Form::text('name','', ['class'=>'form-control','placeholder' => 'Add Name','style' => ' width: 53%;']) !!} 
                {!! Form::hidden('id','',['id'=>'parentId']) !!}
                <br>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="input-group input-small">
                    <div class="form-control uneditable-input input-fixed input-small" data-trigger="fileinput">
                      <i class="fa fa-file fileinput-exists pull-left"></i>&nbsp;
                      <span class="fileinput-filename"> </span>
                    </div>
                    <span class="input-group-addon btn default btn-file ">
                        <span class="fileinput-new"> Select file </span>
                    <span class="fileinput-exists"> Change </span>
                    <input type="file" name="image"> </span>
                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
                </div>
              </center>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <button type="submit" class="btn green">Confirm</button>
            </form>
          </div>

        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    {{-- model image begin --}}
    <div id="up" class="modal fade in" tabindex="-1" data-width="400">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title"><b> Upload Wallpapers </b></h4>
          </div>
          <div class="modal-body ">
            <div class="image-model-body">
            </div>
            <div class="form-group hidden-post-images">
              <h4 class="text-center" style="color:#428bca;">
                just click to browse<span class="glyphicon glyphicon-hand-down"></span>
              </h4>
              <form name="image" action="{{URL::to('admin/wallpapers')}}" class="dropzone" id="addImages">
                {!! csrf_field() !!}
                <div class="fallback">
                  <input name="file" type="file" multiple />
                </div>
                {!! Form::hidden('id','', ['id'=>'uploadId']) !!}

              </form>
            </div>
            <div class="modal-footer">
              <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
          </div>
          {{-- model image end --}}
        </div>
      </div>
    </div>
  </div>
</div>
@stop 

@section('pagelevel_plugins') 

@endsection 

@section('pagelevel_script')

<script type="text/javascript" src="{{ URL::asset('public/js/treeview.js')}}"></script>

<script type="text/javascript">
  function conformation(id) {
    $('#id').val(id);
  }
  function parent(id) {
    $('#parentId').val(id);
  }
  function closeup(id) {
    $('#closeid').val(id);
  }
  function upload(id) {
    $('#uploadId').val(id);
  }

</script>
<script src="{{URL::asset('admin-asset/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{URL::to('admin-asset/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}">
</script>
@if (session('result') != null) {{-- expr --}}
<script type="text/javascript">
  $(function () {

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    @if(session('result')[1] == 'success')
      toastr.success('<?php $msg = session("result");echo $msg[0] ?>', '{{$msg[1]}}');
    @else
    toastr.error('<?php $msg = session("result");echo $msg[0] ?>', '{{$msg[1]}}');
    @endif
  })

</script>
@endif

<script src="{{URL::to('public/js/dropzone.js')}}"></script>

<script type="text/javascript">
  var id;
  Dropzone.options.addImages = {
    maxFilesize: 15,
    acceptedFiles: 'image/*',
    dictDefaultMessage: " Upload Wallpaper  ",
    success: function (file) {
      if (file.previewElement) {
        return file.previewElement.classList.add("dz-success"),
          $(function () {
            setTimeout(function () {
              $('.dz-success').fadeOut('slow');
            }, 1500);
            ajax_call();
          });


      }
    },

  };

</script>

@endsection
