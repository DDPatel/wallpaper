<div class="page-sidebar-wrapper">

                    <div class="page-sidebar navbar-collapse collapse">

                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->

                            <li class="nav-item {{ Request::path() == 'admin/dashboard' ? 'active open' : ''}}">
                                <a href="{{url::to('admin/dashboard')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                </a>
                            </li>

                            <li class="nav-item {{ Request::path() == 'admin/categories' ? 'active open' : ''}}">
                                <a href="{{url::to('admin/categories')}}" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Categories</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item {{ Request::path() == 'admin/users' ? 'active open' : ''||  Request::path() == 'admin/users/create' ? 'active open' : '' }}">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Users</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ Request::path() == 'admin/users' ? 'active open' : '' }}">
                                        <a href="{{url::to('admin/users')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::path() == 'admin/users/create' ? 'active open' : '' }}">
                                        <a href="{{url::to('admin/users/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item  {{ Request::path() == 'admin/role' ? 'active open' : ''||  Request::path() == 'admin/role/create' ? 'active open' : '' }}">
                               <a href="javascript:;" class="nav-link nav-toggle">
                                   <i class="icon-diamond"></i>
                                   <span class="title">Role</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                               </a>
                               <ul class="sub-menu">
                                   <li class="nav-item {{ Request::path() == 'admin/role' ? 'active open' : '' }}">
                                       <a href="{{url::to('admin/role')}}" class="nav-link ">
                                           <span class="title">View</span>
                                       </a>
                                   </li>
                                   <li class="nav-item {{ Request::path() == 'admin/role/create' ? 'active open' : '' }}">
                                       <a href="{{url::to('admin/role/create')}}" class="nav-link ">
                                           <span class="title">Add</span>
                                       </a>
                                   </li>
                               </ul>
                           </li>




                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
